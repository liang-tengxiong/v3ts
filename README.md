# Vue 3 + TypeScript + Vite

1. npm create vite 创建vite+vue+ts项目
2. 配置eslint
   npm install eslint -D
   npx eslint --init  初始化eslint.cjs 配置文件
3. vue3 环境代码校验插件
   npm install -D eslint-plugin-import eslint-plugin-vue eslint-plugin-node eslint-plugin-prettier eslint-config-prettier eslint-plugin-node @babel/eslint-parser
